import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from 'src/app/models/User.model';
import { UserhttpService } from 'src/app/services/userhttp.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.css']
})
export class EditUserComponent {
  user?: User;
  form: FormGroup = new FormGroup({});

  constructor(public activatedRoute: ActivatedRoute, public router: Router, private userHttpService: UserhttpService,
    public formBuilder: FormBuilder) { }

  ngOnInit(): void {

    const id = this.activatedRoute.snapshot.params['id'];
    this.userHttpService.getUser(id).subscribe((user: User) => {
      this.user = user;
      console.log(user);
    });

    this.form = this.formBuilder.group({
      name: [this.user?.name, Validators.required],
      email: [this.user?.email, [Validators.email, Validators.required]],
      description: [this.user?.description, Validators.required],
      job: [this.user?.job, Validators.required]
    });

  }

  submit(): void {
    const updatedUser = {
      ...this.user,
      ...this.form.value
    }
    this.userHttpService.updateUser(updatedUser).subscribe(() => {
      console.log("l'utilisateur a été mis a jour !");
      this.router.navigateByUrl('/home');
    });

  }


}
