import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from 'src/app/models/User.model';
import { UserhttpService } from 'src/app/services/userhttp.service';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})

export class AddUserComponent {
  user?: User;
  form: FormGroup = new FormGroup({});

  constructor(private userHttpService: UserhttpService, public formBuilder: FormBuilder, private router: Router) { }

  ngOnInit(): void {

    this.form = this.formBuilder.group({
      name: [this.user?.name, Validators.required],
      email: [this.user?.email, [Validators.email, Validators.required]],
      description: [this.user?.description, Validators.required],
      job: [this.user?.job, Validators.required]
    });

  }

  submit(): void {
    const newUser = this.form.value as User;
    this.userHttpService.addUser(newUser).subscribe(() => {
      console.log("l'utilisateur a été ajouté !");
      this.router.navigateByUrl('/home');
    });
  }
}
