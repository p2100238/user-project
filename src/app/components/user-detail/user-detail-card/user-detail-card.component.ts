import { Component, Input } from '@angular/core';
import { User } from 'src/app/models/User.model';

@Component({
  selector: 'app-user-detail-card',
  templateUrl: './user-detail-card.component.html',
  styleUrls: ['./user-detail-card.component.css']
})
export class UserDetailCardComponent {

  @Input() user!: User;

}
