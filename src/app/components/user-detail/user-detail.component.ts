import { Component } from '@angular/core';
import { UserhttpService } from 'src/app/services/userhttp.service';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from 'src/app/models/User.model';


@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.css']
})
export class UserDetailComponent {

  user?: User;
  constructor(public activatedRoute: ActivatedRoute, public router: Router, private UserHttpService: UserhttpService){}

  ngOnInit(): void {
    const id = this.activatedRoute.snapshot.params['id'];
    console.log(id);
    this.UserHttpService.getUser(id).subscribe((user: User) => {
      this.user = user;
    });
  }

}
