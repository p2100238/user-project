import { Component } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { User } from 'src/app/models/User.model';
import { AuthentificationService } from 'src/app/services/authentification.service';
import { UserhttpService } from 'src/app/services/userhttp.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent {

  isLoadingResults = true;
  filterForm: FormGroup = new FormGroup({});
  displayedColumns: string[] = ['id', 'name', 'job', 'email', 'actions'];
  dataSource = new MatTableDataSource<User>([]);

  constructor(public userHttpService: UserhttpService, public authentificationService: AuthentificationService, private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.filterForm = this.formBuilder.group({
      name: [''],
      job: [''],
      email: [''],
    });

    this.filterForm.valueChanges.subscribe(() => {
      this.dataSource.filter = this.filterForm.value;
    });

    this.dataSource.filterPredicate = (record, filter: any) => {
      const { name, job, email } = filter;

      const nameMatch = record.name.toLowerCase().includes(name.toLowerCase());
      const jobMatch = record.job.toLowerCase().includes(job.toLowerCase());
      const emailMatch = record.email.toLowerCase().includes(email.toLowerCase());

      return nameMatch && jobMatch && emailMatch;
    }
  }

  ngAfterViewInit(): void {
    this.userHttpService.getUsers().subscribe(users => {
      this.dataSource.data = users;
      this.isLoadingResults = false;
    });
  }

  delete(user: User) {
    this.userHttpService.deleteUser(user).subscribe(() => {
      this.dataSource.data = this.dataSource.data.filter(u => u.id != user.id);
    });
  }



}
