import { Injectable } from '@angular/core';
import Credentials from '../models/Credentials.model';

@Injectable({
  providedIn: 'root'
})
export class AuthentificationService {

  private authentified = false;

  constructor(){}

  login(credentials: Credentials) {
    const { login, password } = credentials;
    this.authentified = true;
    console.log("User connected");
    return this.authentified;
  }

  logout() {
    this.authentified = false;
    console.log("User disconnected");
  }

  isAuthenticated(): boolean {
    return this.authentified;
  }

}
