import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { User } from '../models/User.model';

@Injectable({
  providedIn: 'root'
})
export class UserhttpService {

  baseUrl = 'https://6588238090fa4d3dabf98106.mockapi.io/api/user';

  constructor(private httpClient: HttpClient) { }

  public getUsers() {
    return this.httpClient.get<User[]>(this.baseUrl);
  }

  public getUser(id: string) {
    return this.httpClient.get<User>(`${this.baseUrl}/${id}`);
  }

  public updateUser(user: User){
    return this.httpClient.put(`${this.baseUrl}/${user.id}`, user);
  }

  public addUser(user: User){
    return this.httpClient.post(this.baseUrl, user);
  }

  public deleteUser(user: User){
    return this.httpClient.delete(`${this.baseUrl}/${user.id}`);
  }

}
