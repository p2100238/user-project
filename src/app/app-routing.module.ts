import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserListComponent } from './components/user-list/user-list.component';
import { EditUserComponent } from './components/edit-user/edit-user.component';
import { UserDetailComponent } from './components/user-detail/user-detail.component';
import { AddUserComponent } from './components/add-user/add-user.component';

const routes: Routes = [
  { path: 'users', component: UserListComponent },
  { path: 'edit/:id', component: EditUserComponent},
  { path: 'detail/:id', component: UserDetailComponent},
  { path: 'add', component: AddUserComponent},
  { path: '**', redirectTo: 'users' },

]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
