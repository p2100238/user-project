export class User {
    id?: number;
    constructor(
       public name: string,
       public email: string,
       public description: string,
       public job: string,
    ) {}
 }
 